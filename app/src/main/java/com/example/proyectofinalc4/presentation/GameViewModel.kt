package com.example.proyectofinalc4.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.example.proyectofinalc4.core.Resource
import com.example.proyectofinalc4.data.model.Game
import com.example.proyectofinalc4.data.model.GamePrediction
import com.example.proyectofinalc4.repository.GameRepository
import kotlinx.coroutines.Dispatchers

class GameViewModel(private val repository: GameRepository) : ViewModel() {
    fun fetchGames() = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(Resource.Success(repository.getGames()))
        } catch (exception: Exception) {
            emit(Resource.Failure(exception))
        }
    }

    fun fetchGamesPredictions() = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(Resource.Success(repository.getGamesPredictions()))
        } catch (exception: Exception) {
            emit(Resource.Failure(exception))
        }
    }

    fun fetchGamesPredictions(userId: String, gameId: Int) = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(Resource.Success(repository.getGamesPredictions(userId, gameId)))
        } catch (exception: Exception) {
            emit(Resource.Failure(exception))
        }
    }

    fun saveGamePrediction(gamePrediction: GamePrediction?) = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(Resource.Success(repository.saveGamePrediction(gamePrediction)))
        } catch (exception: Exception) {
            emit(Resource.Failure(exception))
        }
    }
}

class GameViewModelFactory(private val repository: GameRepository): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(GameRepository::class.java).newInstance(repository)
    }
}