package com.example.proyectofinalc4.ui.games

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.example.proyectofinalc4.R
import com.example.proyectofinalc4.core.Resource
import com.example.proyectofinalc4.data.local.AppDatabase
import com.example.proyectofinalc4.data.local.LocalDataSource
import com.example.proyectofinalc4.data.model.Game
import com.example.proyectofinalc4.data.remote.ApiClient
import com.example.proyectofinalc4.data.remote.FirebaseService
import com.example.proyectofinalc4.data.remote.GameDataSource
import com.example.proyectofinalc4.databinding.FragmentGamesBinding
import com.example.proyectofinalc4.presentation.GameViewModel
import com.example.proyectofinalc4.presentation.GameViewModelFactory
import com.example.proyectofinalc4.repository.GameRepository
import com.example.proyectofinalc4.repository.GameRepositoryImp
import com.example.proyectofinalc4.ui.games.adapters.GameAdapter

class GamesFragment : Fragment(R.layout.fragment_games) {
    private lateinit var binding: FragmentGamesBinding
    private val args by navArgs<GamesFragmentArgs>()
    private lateinit var adapter: GameAdapter

    private val viewModel by viewModels<GameViewModel> {
        GameViewModelFactory(
            GameRepositoryImp(
                LocalDataSource(
                    AppDatabase.getDatabase(this.requireContext()).gameDao(),
                    AppDatabase.getDatabase(this.requireContext()).gamePredictionDao()
                ),
//                GameDataSource(ApiClient.service)
                GameDataSource(ApiClient.service, FirebaseService())
            )
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentGamesBinding.bind(view)

        binding.recyclerGames.layoutManager = GridLayoutManager(this.requireContext(), 2)



        viewModel.fetchGames().observe(viewLifecycleOwner, Observer { result ->
            when (result) {
                is Resource.Loading -> {
                    binding.progressbar.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    binding.progressbar.visibility = View.GONE
                    adapter = GameAdapter(result.data.data) { game ->
                        onGameClick(game)
                    }
                    binding.recyclerGames.adapter = adapter

                    Log.d("LiveData", "${result.data.toString()}")
                }
                is Resource.Failure -> {
                    binding.progressbar.visibility = View.GONE
                    Log.d("LiveData", "${result.exception.toString()}")
                }
            }
        })

        binding.swipeContainer.setOnRefreshListener {
            viewModel.fetchGames().observe(viewLifecycleOwner, Observer { result ->
                when (result) {
                    is Resource.Loading -> {
                        binding.progressbar.visibility = View.VISIBLE
                    }
                    is Resource.Success -> {
                        binding.progressbar.visibility = View.GONE
                        adapter = GameAdapter(result.data.data) { game ->
                            onGameClick(game)
                        }
                        binding.recyclerGames.adapter = adapter

                        Log.d("LiveData", "${result.data.toString()}")
                    }
                    is Resource.Failure -> {
                        binding.progressbar.visibility = View.GONE
                        Log.d("LiveData", "${result.exception.toString()}")
                    }
                }
            })
            binding.swipeContainer.setRefreshing(false)
        }
        // Configure the refreshing colors

        binding.swipeContainer.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        )

    }

    private fun onGameClick(game: Game) {
        if (game.result != -1) {
            Toast.makeText(
                requireContext(),
                "Ya has dado tu predicción a este partido!",
                Toast.LENGTH_LONG
            ).show()
            return
        }
        val action = GamesFragmentDirections.actionGamesFragmentToGameDetailsFragment(
            game.id,
            args.userId,
            game.teamOneId,
            game.teamOneName,
            game.teamOneUrl,
            game.teamTwoId,
            game.teamTwoName,
            game.teamTwoUrl,
            game.result
        )
        findNavController().navigate(action)
    }
}