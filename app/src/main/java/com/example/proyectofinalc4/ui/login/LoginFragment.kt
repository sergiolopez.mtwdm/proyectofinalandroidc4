package com.example.proyectofinalc4.ui.login

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.proyectofinalc4.R
import com.example.proyectofinalc4.databinding.FragmentLoginBinding
import com.example.proyectofinalc4.ui.games.GamesFragmentDirections
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class LoginFragment : Fragment(R.layout.fragment_login) {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private lateinit var auth: FirebaseAuth
    private lateinit var email: String
    private lateinit var password: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //return super.onCreateView(inflater, container, savedInstanceState)
        val TAG = "PRUEBAS"
        Log.d(TAG, "-----> Doing binding in login")
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val TAG = "PRUEBAS"

//        auth = FirebaseAuth.getInstance()
        // Initialize Firebase Auth

        auth = Firebase.auth


        binding.btnAbout.setOnClickListener {
            val action = LoginFragmentDirections.actionLoginFragmentToAboutFragment()
            findNavController().navigate(action)
        }

        binding.btnSigin.setOnClickListener {
            Log.d(TAG, "-----> btnSigin")
            email = binding.txtEmail.text.toString()
            password = binding.txtPassword.text.toString()
            //Toast.makeText(requireContext(), "LOGIN", Toast.LENGTH_LONG).show()

            if (!email.isEmpty() && !password.isEmpty()) {
                auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(requireActivity()) { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success")
                            val user = auth.currentUser
//                            var intent = Intent(this, OrderActivity::class.java)
//                            startActivity(intent)
//                            finish()
                            val action = LoginFragmentDirections.actionLoginFragmentToGamesFragment(
                                user!!.uid
                            )
                            findNavController().navigate(action)
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.exception)
//                            Toast.makeText(requireContext(), "LOGIN", Toast.LENGTH_LONG).show()
                            Toast.makeText(
                                requireContext(),
                                resources.getString(R.string.msg_authentication_failed)
                                    .toString() + ", " + task.exception.toString(),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            } else {
                Toast.makeText(requireContext(), R.string.missing_data, Toast.LENGTH_LONG).show()
            }

        }

    }



    override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        if (currentUser != null) {
            //reload();
            /*Toast.makeText(
                requireContext(),
                "Logeado",
                Toast.LENGTH_LONG
            ).show()*/
//            val action = LoginFragmentDirections.actionLoginFragmentToGamesFragment()
//            findNavController().navigate(action)
        }
    }


}