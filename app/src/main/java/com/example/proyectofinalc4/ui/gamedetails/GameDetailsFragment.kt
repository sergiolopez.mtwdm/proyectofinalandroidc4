package com.example.proyectofinalc4.ui.gamedetails

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.proyectofinalc4.R
import com.example.proyectofinalc4.databinding.FragmentGameDetailsBinding
import com.example.proyectofinalc4.ui.games.GamesFragmentDirections
import com.squareup.picasso.Picasso

class GameDetailsFragment : Fragment(R.layout.fragment_game_details) {

    private lateinit var binding: FragmentGameDetailsBinding
    private val args by navArgs<GameDetailsFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentGameDetailsBinding.bind(view)

        binding.textTitle.text = "Partido #" + args.id.toString()
        binding.txtTeamone.text = args.teamOneName
        binding.txtTeamtwo.text = args.teamTwoName

        binding.btnPredict.setOnClickListener {
            val action =
                GameDetailsFragmentDirections.actionGameDetailsFragmentToGamePredictFragment(
                    args.id,
                    args.userId,
                    args.teamOneName,
                    args.teamTwoName
                )
            findNavController().navigate(action)
        }
        Picasso.get().load(args.teamOneUrl).into(binding.imgTeamone)
        Picasso.get().load(args.teamTwoUrl).into(binding.imgTeamtwo)
    }
}