package com.example.proyectofinalc4.ui.games.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.proyectofinalc4.R
import com.example.proyectofinalc4.data.model.Game
import com.example.proyectofinalc4.databinding.ItemGameBinding
import com.squareup.picasso.Picasso

class GameAdapter (private val games: List<Game>, private val listener:(Game)-> Unit) :
    RecyclerView.Adapter<GameAdapter.ViewHolder>() {

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v){
        val binding = ItemGameBinding.bind(v)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v:View = LayoutInflater.from(parent.context).inflate(R.layout.item_game, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val game = games[position]
        holder.itemView.setOnClickListener {
            listener(game)
        }
        Picasso.get().load(game.teamOneUrl).into(holder.binding.imgTeamone)
        Picasso.get().load(game.teamTwoUrl).into(holder.binding.imgTeamtwo)
    }

    override fun getItemCount(): Int = games.size
}