package com.example.proyectofinalc4.ui.gamedetails

//import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.proyectofinalc4.R
import com.example.proyectofinalc4.core.Resource
import com.example.proyectofinalc4.data.local.AppDatabase
import com.example.proyectofinalc4.data.local.LocalDataSource
import com.example.proyectofinalc4.data.model.GamePrediction
import com.example.proyectofinalc4.data.remote.ApiClient
import com.example.proyectofinalc4.data.remote.FirebaseService
import com.example.proyectofinalc4.data.remote.GameDataSource
import com.example.proyectofinalc4.databinding.FragmentGamePredictBinding
import com.example.proyectofinalc4.presentation.GameViewModel
import com.example.proyectofinalc4.presentation.GameViewModelFactory
import com.example.proyectofinalc4.repository.GameRepositoryImp
import com.example.proyectofinalc4.ui.games.adapters.GameAdapter


class GamePredictFragment : Fragment(R.layout.fragment_game_predict) {

    private lateinit var binding: FragmentGamePredictBinding
    private val args by navArgs<GamePredictFragmentArgs>()

    private val viewModel by viewModels<GameViewModel> {
        GameViewModelFactory(
            GameRepositoryImp(
                LocalDataSource(
                    AppDatabase.getDatabase(this.requireContext()).gameDao(),
                    AppDatabase.getDatabase(this.requireContext()).gamePredictionDao()
                ),
//                GameDataSource(ApiClient.service)
                GameDataSource(ApiClient.service, FirebaseService())
            )
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentGamePredictBinding.bind(view)

//        setSupportActionBar(binding.toolbar)
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        var gamePrediction = GamePrediction("", args.id, args.userId, 0)
        binding.btnLocal.text = args.teamOneName
        binding.btnVisit.text = args.teamTwoName

        viewModel.fetchGamesPredictions(gamePrediction.userId, gamePrediction.gameId)
            .observe(viewLifecycleOwner, Observer { result ->
                when (result) {
                    is Resource.Loading -> {
                        binding.progressbar.visibility = View.VISIBLE
                    }
                    is Resource.Success -> {
                        binding.progressbar.visibility = View.GONE
                        if (result.data.data.size > 0) {
                            Toast.makeText(
                                requireContext(),
                                "Ya has seleccionado un resultado",
                                Toast.LENGTH_LONG
                            ).show()
                            Log.d("PRUEBAS", "${result.data.toString()}")
                            findNavController().popBackStack()
                        } else {
                            Toast.makeText(
                                requireContext(),
                                "Estas adivinando en el juego ${args.id}!",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                    is Resource.Failure -> {
                        binding.progressbar.visibility = View.GONE
                        Log.d("PRUEBAS", "${result.exception.toString()}")
                    }
                }
            })

        //Toast.makeText(requireContext(), "Empate!", Toast.LENGTH_LONG).show()
        binding.btnLocal.setOnClickListener {
            gamePrediction.guess = 1
            viewModel.saveGamePrediction(gamePrediction)
                .observe(viewLifecycleOwner, Observer { result ->
                    when (result) {
                        is Resource.Loading -> {
                            binding.progressbar.visibility = View.VISIBLE
                        }
                        is Resource.Success -> {
                            binding.progressbar.visibility = View.GONE
                            findNavController().popBackStack()
//                        Log.d("LiveData", "${result.data.toString()}")
                        }
                        is Resource.Failure -> {
                            binding.progressbar.visibility = View.GONE
                            binding.progressbar.visibility = View.GONE
                            Toast.makeText(
                                requireContext(),
                                "${result.exception.toString()}",
                                Toast.LENGTH_LONG
                            ).show()
//                        Log.d("LiveData", "${result.exception.toString()}")
                        }
                    }
                })
        }
        binding.btnDraw.setOnClickListener {
            viewModel.saveGamePrediction(gamePrediction)
                .observe(viewLifecycleOwner, Observer { result ->
                    when (result) {
                        is Resource.Loading -> {
                            binding.progressbar.visibility = View.VISIBLE
                        }
                        is Resource.Success -> {
                            binding.progressbar.visibility = View.GONE
                            findNavController().popBackStack()
//                        Log.d("LiveData", "${result.data.toString()}")
                        }
                        is Resource.Failure -> {
                            binding.progressbar.visibility = View.GONE
                            binding.progressbar.visibility = View.GONE
                            Toast.makeText(
                                requireContext(),
                                "${result.exception.toString()}",
                                Toast.LENGTH_LONG
                            ).show()
//                        Log.d("LiveData", "${result.exception.toString()}")
                        }
                    }
                })
        }
        binding.btnVisit.setOnClickListener {
            gamePrediction.guess = 2
            viewModel.saveGamePrediction(gamePrediction)
                .observe(viewLifecycleOwner, Observer { result ->
                    when (result) {
                        is Resource.Loading -> {
                            binding.progressbar.visibility = View.VISIBLE
                        }
                        is Resource.Success -> {
                            binding.progressbar.visibility = View.GONE
                            findNavController().popBackStack()
//                        Log.d("LiveData", "${result.data.toString()}")
                        }
                        is Resource.Failure -> {
                            binding.progressbar.visibility = View.GONE
                            binding.progressbar.visibility = View.GONE
                            Toast.makeText(
                                requireContext(),
                                "${result.exception.toString()}",
                                Toast.LENGTH_LONG
                            ).show()
//                        Log.d("LiveData", "${result.exception.toString()}")
                        }
                    }
                })
        }
    }
}