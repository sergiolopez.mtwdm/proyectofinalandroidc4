package com.example.proyectofinalc4.repository

import com.example.proyectofinalc4.data.model.Game
import com.example.proyectofinalc4.data.model.GameList
import com.example.proyectofinalc4.data.model.GamePrediction
import com.example.proyectofinalc4.data.model.GamePredictionList

interface GameRepository {
    suspend fun getGames(): GameList
    suspend fun getGamesPredictions(): GamePredictionList
    suspend fun getGamesPredictions(userId: String, gameId: Int): GamePredictionList
    suspend fun saveGamePrediction(gamePrediction: GamePrediction?): GamePrediction?
}