package com.example.proyectofinalc4.repository

import com.example.proyectofinalc4.data.local.LocalDataSource
import com.example.proyectofinalc4.data.model.*
import com.example.proyectofinalc4.data.remote.GameDataSource

class GameRepositoryImp(
    private val localDataSource: LocalDataSource,
    private val dataSource: GameDataSource
) : GameRepository {

    override suspend fun getGames(): GameList {
        dataSource.getGames().data.forEach { game ->
            localDataSource.saveGame(game.toGameEntity())
        }
        return localDataSource.getGames()
    }

    //    override suspend fun getGamesPredictions(): GamePredictionList {
//        dataSource.getGamesPredictions().data.forEach { gamePrediction ->
//            localDataSource.saveGamePrediction(gamePrediction.toGamePredictionEntity())
//        }
//        return localDataSource.getGamesPredictions()
//    }
    override suspend fun getGamesPredictions(): GamePredictionList = dataSource.getGamesPredictions()

    override suspend fun getGamesPredictions(userId: String, gameId: Int): GamePredictionList = dataSource.getGamesPredictions(userId, gameId)

//    override suspend fun saveGamePrediction(gamePrediction: GamePrediction?): GamePrediction? {
//        return dataSource.saveGamePrediction(gamePrediction)
//    }

    //    override suspend fun getGames(): GameList = dataSource.getGames()
//    override suspend fun getGamesPredictions(): GamePredictionList = dataSource.getGamesPredictions()
    override suspend fun saveGamePrediction(gamePrediction: GamePrediction?): GamePrediction? =
        dataSource.saveGamePrediction(gamePrediction)
}