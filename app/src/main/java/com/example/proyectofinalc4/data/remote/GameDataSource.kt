package com.example.proyectofinalc4.data.remote

import com.example.proyectofinalc4.data.extensions.toGamePredictionList
import com.example.proyectofinalc4.data.model.Game
import com.example.proyectofinalc4.data.model.GameList
import com.example.proyectofinalc4.data.model.GamePrediction
import com.example.proyectofinalc4.data.model.GamePredictionList
import kotlinx.coroutines.tasks.await

//class GameDataSource(private val apiService: ApiService) {
class GameDataSource(private val apiService: ApiService, private val firebase: FirebaseService) {

    suspend fun getGames(): GameList = apiService.getGames()

    //    suspend fun getGamesPredictions(): GamePredictionList = apiService.getGamesPredictions()
//    suspend fun saveGamePrediction(gamePrediction: GamePrediction?) : GamePrediction? = apiService.saveGamePrediction(gamePrediction)
    suspend fun getGamesPredictions(): GamePredictionList =
        firebase.gamesPredictionRef().get().await().toGamePredictionList()

    suspend fun getGamesPredictions(userId: String, gameId: Int): GamePredictionList =
        firebase.gamesPredictionRef().whereEqualTo("userId", userId).whereEqualTo("gameId", gameId)
            .get().await().toGamePredictionList()

    suspend fun saveGamePrediction(gamePrediction: GamePrediction?): GamePrediction? {
        gamePrediction.let {
            firebase.gamePredictionRef().set(gamePrediction!!).await()
        }
        return gamePrediction
    }
}