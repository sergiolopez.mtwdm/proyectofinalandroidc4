package com.example.proyectofinalc4.data.model

data class GamePredictionList (val data:List<GamePrediction> = listOf())