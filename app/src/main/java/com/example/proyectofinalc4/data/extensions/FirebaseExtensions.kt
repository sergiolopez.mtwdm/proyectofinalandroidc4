package com.example.proyectofinalc4.data.extensions

import com.example.proyectofinalc4.data.model.GamePrediction
import com.example.proyectofinalc4.data.model.GamePredictionList
import com.google.firebase.firestore.QuerySnapshot

fun QuerySnapshot.toGamePredictionList(): GamePredictionList {
    var lst = mutableListOf<GamePrediction>()
    for(document in this){
        var gamePrediction = document.toObject(GamePrediction::class.java)
        lst.add(gamePrediction)
    }
    return GamePredictionList(lst)
}