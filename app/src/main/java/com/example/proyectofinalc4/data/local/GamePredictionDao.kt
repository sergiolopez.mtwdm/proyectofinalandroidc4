package com.example.proyectofinalc4.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.proyectofinalc4.data.model.GamePredictionEntity

@Dao
interface GamePredictionDao {
    @Query("SELECT * FROM GamePredictionEntity")
    suspend fun getGamesPredictions(): List<GamePredictionEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveGamePrediction(gamePrediction: GamePredictionEntity)
}