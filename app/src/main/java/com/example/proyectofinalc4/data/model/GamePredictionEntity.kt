package com.example.proyectofinalc4.data.model

import androidx.room.Entity
import androidx.room.ColumnInfo
import androidx.room.PrimaryKey

@Entity(tableName = "gamePredictionEntity")
data class GamePredictionEntity(
    @PrimaryKey
    val id: Int = 0,
    @ColumnInfo(name = "gameId")
    val gameId: Int = 0,
    @ColumnInfo(name = "userId")
    val userId: Int = 0,
    @ColumnInfo(name = "guess")
    val guess: Int = 0,
)