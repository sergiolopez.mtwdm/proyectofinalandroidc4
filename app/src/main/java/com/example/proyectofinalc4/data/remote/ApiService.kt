package com.example.proyectofinalc4.data.remote

import com.example.proyectofinalc4.data.model.Game
import com.example.proyectofinalc4.data.model.GameList
import com.example.proyectofinalc4.data.model.GamePrediction
import com.example.proyectofinalc4.data.model.GamePredictionList
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {
    @GET("games")
    suspend fun getGames(): GameList

    @GET("gamePrediction")
    suspend fun getGamesPredictions(): GamePredictionList

    @POST("gamePrediction")
    suspend fun saveGamePrediction(@Body gamePrediction: GamePrediction?) : GamePrediction?
}