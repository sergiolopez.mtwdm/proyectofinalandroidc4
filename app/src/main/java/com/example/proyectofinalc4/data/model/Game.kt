package com.example.proyectofinalc4.data.model

data class Game(
    val id: Int = 0,
    val teamOneId: Int = 0,
    val teamOneName: String,
    val teamOneUrl: String,
    val teamTwoId: Int = 0,
    val teamTwoName: String,
    val teamTwoUrl: String,
    val result: Int = -1
)