package com.example.proyectofinalc4.data.local

import com.example.proyectofinalc4.data.model.*

class LocalDataSource(
    private val gameDao: GameDao,
    private val gamePredictionDao: GamePredictionDao
) {
    suspend fun getGames(): GameList = gameDao.getGames().toGameList()
    suspend fun saveGame(game: GameEntity) = gameDao.saveGame(game)

//    suspend fun getGamesPredictions(): GamePredictionList =
//        gamePredictionDao.getGamesPredictions().toGamePredictionList()
//
//    suspend fun saveGamePrediction(gamePrediction: GamePredictionEntity) =
//        gamePredictionDao.saveGamePrediction(gamePrediction)
}