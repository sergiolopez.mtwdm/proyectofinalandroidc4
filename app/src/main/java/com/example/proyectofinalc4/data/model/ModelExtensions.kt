package com.example.proyectofinalc4.data.model

fun List<GameEntity>.toGameList(): GameList {
    val list = mutableListOf<Game>()
    this.forEach { gameEntity ->
        list.add(gameEntity.toGame())
    }
    return GameList(list)
}

fun Game.toGameEntity(): GameEntity = GameEntity(
    this.id,
    this.teamOneId,
    this.teamOneName,
    this.teamOneUrl,
    this.teamTwoId,
    this.teamTwoName,
    this.teamTwoUrl,
    this.result
)

fun GameEntity.toGame(): Game = Game(
    this.id,
    this.teamOneId,
    this.teamOneName,
    this.teamOneUrl,
    this.teamTwoId,
    this.teamTwoName,
    this.teamTwoUrl,
    this.result
)

//fun List<GamePredictionEntity>.toGamePredictionList(): GamePredictionList {
//    val list = mutableListOf<GamePrediction>()
//    this.forEach { GamePredictionEntity ->
//        list.add(GamePredictionEntity.toGamePrediction())
//    }
//    return GamePredictionList(list)
//}

//fun GamePrediction.toGamePredictionEntity(): GamePredictionEntity = GamePredictionEntity(
//    this.id,
//    this.gameId,
//    this.userId,
//    this.guess
//)
//
//fun GamePredictionEntity.toGamePrediction(): GamePrediction = GamePrediction(
//    this.id,
//    this.gameId,
//    this.userId,
//    this.guess
//)