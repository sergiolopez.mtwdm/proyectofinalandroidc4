package com.example.proyectofinalc4.data.model

import com.google.firebase.firestore.DocumentId

data class GamePrediction (
//    val id: Int = 0,
    @DocumentId
    val id:String = "",
    val gameId: Int = 0,
    val userId: String = "",
    var guess: Int = 0,
)