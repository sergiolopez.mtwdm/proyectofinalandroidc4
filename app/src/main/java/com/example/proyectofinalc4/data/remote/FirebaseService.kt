package com.example.proyectofinalc4.data.remote

import com.google.firebase.firestore.FirebaseFirestore

class FirebaseService {
    fun collectionRef(collection:String) = FirebaseFirestore.getInstance().collection(collection)
    fun gamesPredictionRef() = collectionRef("gamePrediction")
    fun gamePredictionRef() = gamesPredictionRef().document()
}