package com.example.proyectofinalc4.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.proyectofinalc4.data.model.GameEntity

@Dao
interface GameDao {
    @Query("SELECT * FROM gameEntity")
    suspend fun getGames(): List<GameEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveGame(game: GameEntity)

}