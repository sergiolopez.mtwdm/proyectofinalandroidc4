package com.example.proyectofinalc4.data.model

import androidx.room.Entity
import androidx.room.ColumnInfo
import androidx.room.PrimaryKey

@Entity(tableName = "gameEntity")
data class GameEntity(
    @PrimaryKey
    val id: Int = 0,
    @ColumnInfo(name = "teamOneId")
    val teamOneId: Int = 0,
    @ColumnInfo(name = "teamOneName")
    val teamOneName: String,
    @ColumnInfo(name = "teamOneUrl")
    val teamOneUrl: String,
    @ColumnInfo(name = "teamTwoId")
    val teamTwoId: Int = 0,
    @ColumnInfo(name = "teamTwoName")
    val teamTwoName: String,
    @ColumnInfo(name = "teamTwoUrl")
    val teamTwoUrl: String,
    @ColumnInfo(name = "result")
    val result: Int = -1
)